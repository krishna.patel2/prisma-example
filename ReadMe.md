### How I setup,

1. npm init
2. npm install prisma typescript ts-node @types/node --save-dev
3. Initial your repo with, 
    ``npx prisma init``

- Migrations + Update Database:
    - npx prisma migrate dev --name MigrationName
- ONLY Update Migration
    - npx prisma migrate dev --create-only --name MigrationName
- Update Database
    - npx prisma migrate dev